#/bin/bash

while read line
do
curl -k -o qg_name.json -u 'sonar-api':'ck00rU92GP0WXA' https://devsupport-sonar.it.global.hsbc:9009/sonar/api/qualitygates/get_by_project?project=${line}
name=$(cat qg_name.json | /sonar_01/jq/1.5/jq -r ".qualityGate.name")

curl -k -o qg_status.json -u 'sonar-api':'ck00rU92GP0WXA' https://devsupport-sonar.it.global.hsbc:9009/sonar/api/qualitygates/project_status?projectKey=${line}
status=$(cat qg_status.json | /sonar_01/jq/1.5/jq -r ".projectStatus.status")

echo "'"${line}"'" "'"$name"'" "'"$status"'" >> /sonar_01/script/qg.log
done <kee.log
